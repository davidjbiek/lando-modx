# Lando MODX

#### MODX Revolution is the world�s fastest, most secure, flexible and scalable Open Source CMS.

[![LICENSE](https://img.shields.io/badge/License-GPL%20v2-blue.svg)](./LICENSE) [![Build Status](https://github.com/modxcms/revolution/workflows/CI/badge.svg?branch=2.x)](https://github.com/modxcms/revolution/actions?query=branch%3A2.x) [![Contributors](https://img.shields.io/github/contributors/modxcms/revolution.svg)](https://github.com/modxcms/revolution/graphs/contributors) [![Slack Chat](https://img.shields.io/badge/chat_in_slack-online-green.svg?longCache=true&style=flat&logo=slack)](https://modx.org) [![follow on Twitter](https://img.shields.io/twitter/follow/modx.svg?style=social&logo=twitter)](https://twitter.com/intent/follow?screen_name=modx)

## Introduction
This project spins up a fresh install of the latest build of MODX Revolution 2 in Lando.  
**Important:** You will need to have both [Lando](https://lando.dev) and [Composer](https://getcomposer.org/) downloaded and installed in order for this project to run properly.

## Installation
From inside the base directory of this repository on your local machine, simply run:

```sh
$ lando start
$ lando modx:init
```

At this point, you can browse to http://modx.lndo.site/setup to set up MODX as you would in any other environment.  
**Important:** You must change the database name from "localhost" to "database" to connect the local appserver to the database server. 

To use Mailhog, update the smtp port in MODX System Settings from 587 to 1025.  You can view email messages at [http://mail.modx.lndo.site/](http://mail.modx.lndo.site/)

## Initial configuration ##
### Absolute paths ###
To set up absolute paths and friendly URLs the Lippert way: 

* Login to the MODX Manager and go to System Settings.
* Type "link_tag" into the Search by key... search field
* Update the link_tag_scheme field by double-clicking where it says "-1" and changing it to "abs"
* Next, search for "friendly" and look for "Use Friendly URLs" - change this field from "No" to "Yes"

### Removing the HTML suffix from resources ###
* To omit ".html" from all web pages served, navigate to the MODX Manager and find the Content drop-down at the top.
* Click on Content Types
* Under File Extension for row ID 1 (HTML), change the value from ".html" to nothing by double-clicking on the field and backspacing out the contents

## Upgrading
Occasionally, the MODX team will release periodic updates to the CMS. To upgrade to the latest release, simply run:

```sh
$ lando modx:upgrade
```

At this point, you can browse to http://modx.lndo.site/setup to complete the MODX setup process as you would in any other environment.